/*
  MIT License

  Copyright (c) 2018 Mina Helmi

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without limitation
  the rights to use, copy, modify, merge, publish, distribute, sublicense,
  and/or sell copies of the Software, and to permit persons to whom the
  Software is furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "INT_functions.h"
#include "../Peripherals_addresses.h"
#include "../../basic_includes/bit_manip.h"
#include <avr/interrupt.h>


// --- internal --- //
// external interrupts callbacks
static INT_CB_t INT_CB_INT0 = 0;
static INT_CB_t INT_CB_INT1 = 0;
static INT_CB_t INT_CB_INT2 = 0;
// ---------------- //

inline void INT_vidEnable_global_flag(void)
{
	BIT_SET(MCU_STATUS, 7);
}

inline void INT_vidDisable_global_flag(void)
{
	BIT_CLEAR(MCU_STATUS, 7);
}

inline void INT_vidEnable_external_INT(const u8 u8INTnumCpy)
{
	switch (u8INTnumCpy)
	{
		case 0: // INT0
			BIT_SET(MCU_GENERAL_INT_CTRL, 6);
		break;

		case 1: // INT1
			BIT_SET(MCU_GENERAL_INT_CTRL, 7);
		break;

		case 2: // INT2
			BIT_SET(MCU_GENERAL_INT_CTRL, 5);
		break;
	}
}

inline void INT_vidDisable_external_INT(const u8 u8INTnumCpy)
{
	switch (u8INTnumCpy)
	{
		case 0: // INT0
			BIT_CLEAR(MCU_GENERAL_INT_CTRL, 6);
		break;

		case 1: // INT1
			BIT_CLEAR(MCU_GENERAL_INT_CTRL, 7);
		break;

		case 2: // INT2
			BIT_CLEAR(MCU_GENERAL_INT_CTRL, 5);
		break;
	}
}

void INT_vidSet_trigger_type(INT_trigger_t enumTriggerTypeCpy, const u8 u8INTnumCpy)
{
	switch (u8INTnumCpy)
	{
		case 0: // INT0 (bit0-1)
			MCU_CTRL &= 0b11111100; // clear required 2 bits
			MCU_CTRL |= enumTriggerTypeCpy; // set required 2 bits
		break;

		case 1: // INT1 (bit2-3)
			MCU_CTRL &= 0b11110011; // clear required 2 bits
			MCU_CTRL |= (enumTriggerTypeCpy << 2); // set required 2 bits
		break;

		case 2: // INT2
			if (enumTriggerTypeCpy == INT_trigger_Falling_edge)
				BIT_CLEAR(MCU_CTRL_STATUS, 6);
			else if (enumTriggerTypeCpy == INT_trigger_Rising_edge)
				BIT_SET(MCU_CTRL_STATUS, 6);
		break;
	}
}

inline void INT_vidRegisterCB(const INT_CB_t CBfuncCpy, const u8 u8INTnumCpy)
{
    switch (u8INTnumCpy)
    {
        case 0: // INT0
            INT_CB_INT0 = CBfuncCpy;
        break;

        case 1: // INT1
            INT_CB_INT1 = CBfuncCpy;
        break;

        case 2: // INT2
            INT_CB_INT2 = CBfuncCpy;
        break;
    }
}

inline void INT_vidDeregisterCB(const u8 u8INTnumCpy)
{
    switch (u8INTnumCpy)
    {
        case 0: // INT0
            INT_CB_INT0 = 0;
        break;

        case 1: // INT1
            INT_CB_INT1 = 0;
        break;

        case 2: // INT2
            INT_CB_INT2 = 0;
        break;
    }
}


ISR(INT0_vect)
{
    if (INT_CB_INT0)
        INT_CB_INT0();
}

ISR(INT1_vect)
{
    if (INT_CB_INT1)
        INT_CB_INT1();
}

ISR(INT2_vect)
{
    if (INT_CB_INT2)
        INT_CB_INT2();
}

