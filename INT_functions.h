/*
  MIT License

  Copyright (c) 2018 Mina Helmi

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without limitation
  the rights to use, copy, modify, merge, publish, distribute, sublicense,
  and/or sell copies of the Software, and to permit persons to whom the
  Software is furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef MCAL_DRIVERS_INT_DRIVER_INT_FUNCTIONS_H_
#define MCAL_DRIVERS_INT_DRIVER_INT_FUNCTIONS_H_


#include "../../basic_includes/custom_types.h"

// external INT0-1 trigger type
typedef enum
{
	INT_trigger_Low_level    = 0b00000000,
	INT_trigger_Falling_edge = 0b00000010,
	INT_trigger_Rising_edge  = 0b00000011,
	INT_trigger_Any_chnage   = 0b00000001
} INT_trigger_t;

typedef void (*INT_CB_t)(void); // external interrupts callback type

void INT_vidEnable_global_flag(void);
void INT_vidDisable_global_flag(void);

void INT_vidEnable_external_INT(const u8 u8INTnumCpy);
void INT_vidDisable_external_INT(const u8 u8INTnumCpy);

void INT_vidSet_trigger_type(INT_trigger_t enumTriggerTypeCpy, const u8 u8INTnumCpy);

void INT_vidRegisterCB(const INT_CB_t CBfuncCpy, const u8 u8INTnumCpy);
void INT_vidDeregisterCB(const u8 u8INTnumCpy);


#endif /* MCAL_DRIVERS_INT_DRIVER_INT_FUNCTIONS_H_ */

